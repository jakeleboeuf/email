import { test, skip } from 'qunit';
import moduleForAcceptance from 'email/tests/helpers/module-for-acceptance';
import Mirage from 'ember-cli-mirage';

moduleForAcceptance('Acceptance | contacts edit');

test('Edit contact displays contact current info', function(assert) {
  // set up test data
  let emails = server.createList('contactEmails', 3);
  let phones = server.createList('contactPhones', 2);

  // Surly this is not the correct way to do this...
  server.db.contactPhones.update(1, {
    number: '+15555555252'
  });
  server.db.contactPhones.update(2, {
    number: '+15555550002'
  });
  server.db.contactEmails.update(1, {
    address: 'smith@example.com'
  });
  server.db.contactEmails.update(2, {
    address: 'smith@example.org'
  });
  server.db.contactEmails.update(3, {
    address: 'asmith@example.edu'
  });


  let contact = server.create('contact', { name: 'Ann Smith', contactEmails: emails, contactPhones: phones });

  visitEditContact(contact);

  andThen(function() {
    assert.contains('.test-contact-name input', 'Ann Smith', 'Contact name is displayed');
    assert.contains('.test-contact-phone input', '+15555555252', 'Contact primary phone is displayed');
    assert.contains('.test-contact-phone input', '+15555550002', 'Contact alternate phone is displayed');
    assert.contains('.test-contact-email input', 'smith@example.com', 'Contact primary email is displayed');
    assert.contains('.test-contact-email input', 'smith@example.org', 'Contact alternate email is displayed');
    assert.contains('.test-contact-email input', 'asmith@example.edu', 'Contact work email is displayed');
  });
});

test('Edit contact allows editing contact info', function(assert) {
  // set up test data
  let emails = server.createList('contactEmails', 1);
  let phones = server.createList('contactPhones', 1);
  let contact = server.create('contact', { name: 'Ann Smith', contactEmails: emails, contactPhones: phones});
  let testName = 'Jake LeBoeuf';
  let testPhone = '(865) 242-1019';
  let testEmail = 'design@jakeleboeuf.com';

  visitEditContact(contact);

  // test actions
  fillIn('.test-contact-name input', testName);
  fillIn('.test-contact-phone input:eq(0)', testPhone);
  fillIn('.test-contact-email input:eq(0)', testEmail);

  andThen(function() {
    // fill in the assertions

    assert.equal(find('.test-contact-name input').val(), testName, 'Contact name is updated');

    assert.equal(find('.test-contact-phone:eq(0) input').val(), testPhone, 'Primary phone is updated');

    assert.equal(find('.test-contact-email:eq(0) input').val(), testEmail, 'Primary email is updated');
  });
});

test('Edit contact handles server error when saving contact', function(assert) {
  // set up test data
  let emails = server.createList('contactEmails', 1);
  let phones = server.createList('contactPhones', 1);
  let contact = server.create('contact', { name: 'Ann Smith', contactEmails: emails, contactPhones: phones});

  let testName = 'Jake LeBoeuf';
  let testPhone = '(865) 242-1019';
  let testEmail = 'design@jakeleboeuf.com';

  let origName = 'Ann Smith';
  let origPhone = '+15555555252';
  let origEmail = 'smith@example.com';

  server.db.contactPhones.update(1, {
    number: '+15555555252'
  });
  server.db.contactEmails.update(1, {
    address: 'smith@example.com'
  });

  visitEditContact(contact);


  click('.submit');

  andThen(function() {
    // fill in assertions

    assert.equal(find('.test-contact-name input').val(), origName, 'Contact name is NOT updated');

    assert.equal(find('.test-contact-phone input').val(), origPhone, 'Primary phone is NOT updated');

    assert.equal(find('.test-contact-email input').val(), origEmail, 'Primary email is NOT updated');

    assert.contains('.alert-danger', 'There was an error saving this contact. Please try again.');
  });
});

skip('Edit contact handles server error when saving contact phone, saving all related models other than the erroring one');

skip('Edit contact handles server error when saving contact email, saving all related models other than the erroring one');

skip('Edit contact does not allow empty name');

skip('Edit contact allows adding a phone');
skip('Edit contact allows adding an email');
skip('Edit contact allows adding multiple phones and/or emails at the same time');
skip('Edit contact allows removing emails');
skip('Edit contact allows removing phones');

// contact is a mirage factory
function visitEditContact(contact) {
  visit(`/contacts/${contact.id}`);
}
