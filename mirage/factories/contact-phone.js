import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  number() { return faker.phone.phoneNumber(); },
  primary() { return faker.random.boolean(); }
});
