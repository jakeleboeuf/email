import { Model } from 'ember-cli-mirage';
import DS from 'ember-data';

export default Model.extend({
  address: DS.attr('string'),
  contact: DS.belongsTo()
});
