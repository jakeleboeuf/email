import { Model, hasMany } from 'ember-cli-mirage';

export default Model.extend({
  name: DS.attr('string'),
  contactEmails: hasMany(),
  contactPhones: hasMany()
});
