import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),

  name: null,
  contactEmails: null,
  contactPhones: null,

  actions: {
    create() {
      let errors = Ember.isEmpty(this.get('name')) ||
        Ember.isEmpty(this.get('contactEmails')) ||
        Ember.isEmpty(this.get('contactPhones'));

      if (errors) {
        this.set('error', true);
        return;
      }

      let email = this.get('store').createRecord('contact', this.getProperties('name', 'contactEmails', 'contactPhones'));
      email.save();
    }
  }
});
