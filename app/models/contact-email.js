import DS from 'ember-data';

export default DS.Model.extend({
  address: DS.attr('string'),
  primary: DS.attr('boolean')
});
