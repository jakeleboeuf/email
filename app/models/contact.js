import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
  name: DS.attr('string'),
  contactEmails: DS.hasMany(),
  primaryEmail: Ember.computed('contactEmails', function() {
    let emails = this.get('contactEmails');
    //console.log(emails.filterBy('primary', true).firstObject);
    return emails.firstObject;
  }),
  contactPhones: DS.hasMany()
});
